[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/AlbaRicondo%2Fwrapswash-1d/HEAD)

# wrapSWASH 1D

This toolbox has been developed to ease numerical model integration of the non-hydrostatic wave resolving model SWASH 1D with python code.
It contains custom functions for numerical cases generation, execution, and output postprocessing.


## Main contents

[lib](./lib/): SWASH numerical model toolbox 
- [io](./lib/io.py): SWASH numerical model input/output operations
- [wrap](./lib/wrap.py): SWASH numerical model python wrap
- [plots](./lib/plots.py): plotting library

[scripts](./scripts/): jupyter notebooks to run SWASH
-[Swash](./scripts/Swash_case.ipynb): Notebook compiler for running a sea-state over a cross-shore profile
-[HySwash](./scripts/HySwash.ipynb): Metamodel toolbox (MDA - SWASH - RBF) to simulate wave transformation over a coral reef-lined profile 

## Documentation
SWASH numerical model detailed documentation can be found at: http://swash.sourceforge.net/

- [SWASH install/compile manual](http://swash.sourceforge.net/download/download.htm)
- [SWASH user manual](http://swash.sourceforge.net/online_doc/swashuse/swashuse.html)


## Install
The source code is currently hosted on GitLab at: https://gitlab.com/geoocean/bluemath/mw\_deltares

### Install from sources

Install requirements. Navigate to the base root of [wrapswash-1d](./) and execute:

```
   conda env create -f environment.yml
```

## License

This project is licensed under the MIT License - see the [license](./LICENSE.txt) file for details

